# <img height="25" src="./chat.png"> Chat 

nick <user_name> - задава дадено потребителско име на текущия клиент

send <user_name> <message_content> - изпраща лично съобщение до даден активен потребител

send-all <message_content> - изпраща съобщение до всички активни потребители

list-users – извежда списък с всички активни в момента потребители

disconnect – потребителят напуска чата